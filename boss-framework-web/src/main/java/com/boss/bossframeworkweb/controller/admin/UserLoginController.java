package com.boss.bossframeworkweb.controller.admin;

import com.baomidou.mybatisplus.extension.api.R;
import com.boss.bossframeworkweb.config.Msg;
import com.boss.bossframeworkweb.config.ResponseUtil;
import com.boss.bossframeworkweb.config.utils.BodyType;
import com.boss.bossframeworkweb.config.utils.CCPRestSmsSDK;
import com.boss.bossframeworkweb.pojo.*;
import com.boss.bossframeworkweb.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * 用户登录模块
 */
@Controller
public class UserLoginController {

    //全局短信验证码
    private String datas;

    //用户实现层
    @Autowired
    private IUserLoginService userService;

    //下拉列表
    @Resource
    private JobService jobService;

    //热门职位
    @Resource
    private PositionGzService positionGzService;

    //热门搜索
    @Resource
    private positionsService positionsService;

    //广告展示
    @Resource
    private ImageAdsService imageAds;

    /**
     * 用户登录
     * @param SysUser
     * @return
     */
    @PostMapping("/logon")
    public String login(UserLogin SysUser, HttpServletRequest request) {
        UserLogin user = userService.findUserVoByUsernameAndPassword(SysUser);
        if (user != null) {
            request.getSession().setAttribute("Msg",user.getPhone());
            System.out.println(user.getPhone());
            /*左部分下拉列表*/
            List<job1> l=jobService.list();
            request.setAttribute("json",l);
            /*下部分热门职位*/
            List<PositionGz>list=positionGzService.positionGzlist();
            List<PositionGz>list2=positionGzService.positionGzlist2();
            request.setAttribute("polist",list);
            request.setAttribute("polist1",list2);
            /*热门搜索*/
            List<positions> list3= positionsService.getlist();
            request.setAttribute("relist",list3);
            /*广告图片*/
            List<ImageAds> imageAds = this.imageAds.FindImage();
            request.setAttribute("image",imageAds);
           return "/index";
        }
        request.setAttribute("errorMsg","登录失败");
        return "/login";
    }


    /**
     * 用户注册
     * @param SysUser
     * @return
     */
    @PostMapping("/register")
    public String register(UserLogin SysUser, HttpServletRequest request) {
        System.out.println("前台输入验证码测试："+SysUser.getMsg());
        if (SysUser.getMsg().equals(datas)){
               userService.addPhone(SysUser);
                System.out.println("注册成功！");
            /*左部分下拉列表*/
            List<job1> l=jobService.list();
            request.setAttribute("json",l);
            /*下部分热门职位*/
            List<PositionGz>list=positionGzService.positionGzlist();
            List<PositionGz>list2=positionGzService.positionGzlist2();
            request.setAttribute("polist",list);
            request.setAttribute("polist1",list2);
            /*热门搜索*/
            List<positions> list3= positionsService.getlist();
            request.setAttribute("relist",list3);
            /*广告图片*/
            List<ImageAds> imageAds = this.imageAds.FindImage();
            request.setAttribute("image",imageAds);
                return "/index";
        }
        request.setAttribute("errorMsg","注册失败");
        return "/register";
    }


    /**
     * 短信功能
     * @param phone
     * @return
     */
    @RequestMapping(value = "/note")
    public Msg smsSdk(UserLogin phone) {
        System.out.println("手机号:"+phone.getPhone());
        CCPRestSmsSDK sdk = new CCPRestSmsSDK();
        sdk.init("app.cloopen.com", "8883");
        sdk.setAccount("8a216da87249b81301726e9eb8af10ea", "9fb1ba8368b2496da83c8e02a2a86184");
        sdk.setAppId("8a216da87249b81301726e9eb9a710f0");
        sdk.setBodyType(BodyType.Type_JSON);
        //生成短信随机数
        datas = String.valueOf(((new Random()).nextInt(899999)) + 100000);
        System.out.println("短信随机数"+datas);
        HashMap<String, Object> result = sdk.sendTemplateSMS(phone.getPhone(),"1",new String[]{datas,"5"});
        if("000000".equals(result.get("statusCode"))){
            return ResponseUtil.success();
        }else{
            //异常返回输出错误码和错误信息
            System.out.println("错误码=" + result.get("statusCode") +" 错误信息= "+result.get("statusMsg"));
            //异常返回输出错误码和错误信息
            return ResponseUtil.fail(-1, "错误码=" + result.get("statusCode") + " 错误信息= " + result.get("statusMsg"));
        }
    }



}
