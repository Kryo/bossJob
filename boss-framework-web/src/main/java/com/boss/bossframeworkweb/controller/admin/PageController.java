
package com.boss.bossframeworkweb.controller.admin;

import com.boss.bossframeworkweb.config.DateUtil2;
import com.boss.bossframeworkweb.mapper.JobSearchMapper;
import com.boss.bossframeworkweb.pojo.*;
import com.boss.bossframeworkweb.service.*;
import com.boss.bossframeworkweb.service.impl.companyServiceimpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.util.List;

@Controller
public class PageController {
    //订阅职位
    @Resource
    private JobSearchMapper jobSearch;
    //下拉列表
    @Resource
    private JobService jobService;

    //热门职位
    @Resource
    private PositionGzService positionGzService;

    //热门搜索
    @Resource
    private positionsService positionsService;

    //广告展示
    @Resource
    private ImageAdsService imageAds;

    //公司
    @Resource
    companyServiceimpl companylistService;
    //省份
    @Resource
    private com.boss.bossframeworkweb.service.provinceService provinceService;
    //城市
    @Resource
    com.boss.bossframeworkweb.service.impl.workplaceServiceimpl workplaceServiceimpl;


    //可以访问templates里面的html  比如 index.html,web-login.html等等
    @GetMapping({"","/login"})
    public String loginController() {
        System.out.println("********************************");
        return "login";
    }

    //可以访问templates里面的html  比如 index.html,web-login.html等等
    @GetMapping("/{pageName}.html")
    public String pageController(@PathVariable("pageName") String pageName, HttpServletRequest request) {
        System.out.println("********************************");
        System.out.println("正在访问："+pageName);
        if (pageName.equals("companylist")){
            /*订阅次数*/
            int count = jobSearch.FindJobSearch();
            request.getSession().setAttribute("msg",count);
            /*公司简历*/
            List<workplace> list = workplaceServiceimpl.workplacelist();
            request.setAttribute("workplacelist", list);
            List<province> list1 = provinceService.getAll();
            request.setAttribute("province", list1);
            int totalCount=companylistService.totalCount(0,0);
            Page page=new Page();
            page.setTotalCount(totalCount);
            request.setAttribute("totalPageCount",page.getTotalPageCount());
            return pageName;
        }else if (pageName.equals("index")){
            /*左部分下拉列表*/
            List<job1> l=jobService.list();
            request.setAttribute("json",l);
            /*下部分热门职位*/

            List<PositionGz>list=positionGzService.positionGzlist();
            List<PositionGz>list2=positionGzService.positionGzlist2();
            request.setAttribute("polist",list);
            request.setAttribute("polist1",list2);

            /*发布时间类型转型 问题:乱码*/
         /*   for (PositionGz positionGz:list2){
                System.out.println(positionGz.getData());
                String a=new DateUtil2().timeUtile(positionGz.getCreationTime());
                positionGz.setDateTime(a);
                System.out.println(a);
            }*/
            /*热门搜索*/
            List<positions> list3= positionsService.getlist();
            request.setAttribute("relist",list3);
            /*广告图片*/
            List<ImageAds> imageAds = this.imageAds.FindImage();
            request.setAttribute("image",imageAds);

            /*查询主页类型搜索框*/


            return pageName;
        }
        return pageName;
    }



}
