package com.boss.bossframeworkweb.controller.admin;

import com.boss.bossframeworkweb.pojo.JobGsjl;
import com.boss.bossframeworkweb.pojo.UserLogin;
import com.boss.bossframeworkweb.service.JobGsjlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * 公司简历
 */
@Controller
public class JobGsjlController {

    @Autowired
    private JobGsjlService jobGsjlService;

    /**
     * 简历新增
     * @return
     */
    @PostMapping("/Gsjl")
    public String login(JobGsjl gsjl,HttpServletRequest request) {
        int i = jobGsjlService.AddJobGsjl(gsjl);
        if ( i!= 0) {
            System.out.println("测试:简历新增成功!");
            request.setAttribute("Msg","新增成功！");
            return "/companylist";
        }
        request.setAttribute("errorMsg","新增失败！");
        return "/create";
    }


}
