package com.boss.bossframeworkweb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.boss.bossframeworkweb.pojo.WorkTitle;

public interface WorkTitleMapper  extends BaseMapper<WorkTitle> {

}
