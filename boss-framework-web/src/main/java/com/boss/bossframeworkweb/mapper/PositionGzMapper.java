package com.boss.bossframeworkweb.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.boss.bossframeworkweb.pojo.PositionGz;
import com.boss.bossframeworkweb.pojo.positions;
import com.boss.bossframeworkweb.pojo.workplace;
import org.apache.ibatis.annotations.Mapper;


import java.util.List;
@Mapper
public interface PositionGzMapper extends BaseMapper<PositionGz> {
    //五表查询
    List<PositionGz> positionGzlist();
    //获取时间
    List<PositionGz>positionGzlist2();
    //公司
    List<workplace>gonglist();
    /**
     * 下一个节点
     */
    List<positions> next();
}
