package com.boss.bossframeworkweb.service.impl;

import com.boss.bossframeworkweb.mapper.positionsMapper;
import com.boss.bossframeworkweb.pojo.positions;
import com.boss.bossframeworkweb.service.positionsService;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class positionServiceimpl implements positionsService {
    @Resource
    positionsMapper positionsMapper;
    @Override
    public List<positions> getlist() {
        return positionsMapper.getlist();
    }

    @Override
    public List<positions> findName() {
        return positionsMapper.getlist();
    }
}
