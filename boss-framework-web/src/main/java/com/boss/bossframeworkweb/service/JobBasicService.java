package com.boss.bossframeworkweb.service;

import com.boss.bossframeworkweb.pojo.JobBasic;
import com.boss.bossframeworkweb.pojo.WorkTitle;

public interface JobBasicService {

    //新增基本信息
    int addJobBasic(JobBasic jobBasic);

}
