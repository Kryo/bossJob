package com.boss.bossframeworkweb.service;

import com.boss.bossframeworkweb.pojo.JobSearch;

public interface JobSearchService {

    /**
     * 订阅职位
     */
    int AddJobSearch(JobSearch jobSearch);

    /**
     * 查询订阅职位总人数
     */
    int FindJobSearch();
}
