package com.boss.bossframeworkweb.service.impl;



import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.boss.bossframeworkweb.mapper.workplaceMapper;
import com.boss.bossframeworkweb.pojo.workplace;
import com.boss.bossframeworkweb.service.workplaceService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class workplaceServiceimpl  extends ServiceImpl<workplaceMapper, workplace> implements workplaceService {
    @Resource
    workplaceMapper workplaceMapper;
    @Override
    public List<workplace> workplacelist() {
        return workplaceMapper.workplacelist();
    }
}
