package com.boss.bossframeworkweb.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.boss.bossframeworkweb.mapper.JobSearchMapper;
import com.boss.bossframeworkweb.mapper.UserLoginMapper;
import com.boss.bossframeworkweb.pojo.JobSearch;
import com.boss.bossframeworkweb.pojo.UserLogin;
import com.boss.bossframeworkweb.service.JobSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class JobSearchServiceImpl extends ServiceImpl<JobSearchMapper, JobSearch> implements JobSearchService {

    private JobSearchMapper jobsearch;

    /**
     * 订阅职位
     * @param jobSearch
     * @return
     */
    @Override
    public int AddJobSearch(JobSearch jobSearch) {
        return baseMapper.insert(jobSearch);
    }


    /**
     * 查询订阅职位总人数
     */
    @Override
    public int FindJobSearch() {
        return jobsearch.FindJobSearch();
    }
}
