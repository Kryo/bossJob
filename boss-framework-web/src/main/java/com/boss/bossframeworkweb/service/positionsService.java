package com.boss.bossframeworkweb.service;

import com.boss.bossframeworkweb.pojo.positions;

import java.util.List;

public interface positionsService {
    //热门职位
    List<positions> getlist();

    //搜索框
    List<positions>findName();
}
