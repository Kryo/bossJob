package com.boss.bossframeworkweb.service.impl;



import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.boss.bossframeworkweb.mapper.companylistMapper;

import com.boss.bossframeworkweb.pojo.Page;
import com.boss.bossframeworkweb.pojo.company;
import com.boss.bossframeworkweb.pojo.development;
import com.boss.bossframeworkweb.pojo.workplace;
import com.boss.bossframeworkweb.service.companylistService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class companyServiceimpl  extends ServiceImpl<companylistMapper, company> implements companylistService {
@Resource
    companylistMapper companylistMapper;


    @Override
    public List<company> companyPageList(Page page) { return companylistMapper.companyPageList(page); }

    @Override
    public int totalCount(Integer dId, Integer fId) { return companylistMapper.totalCount(dId, fId); }
}
