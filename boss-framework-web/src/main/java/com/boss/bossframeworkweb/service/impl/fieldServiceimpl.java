package com.boss.bossframeworkweb.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.boss.bossframeworkweb.mapper.fieldMapper;
import com.boss.bossframeworkweb.pojo.field;
import com.boss.bossframeworkweb.service.fieldService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class fieldServiceimpl extends ServiceImpl<fieldMapper, field> implements fieldService {
    @Resource
    fieldMapper fieldMapper;
    @Override
    public List<field> findAllField() {
        return fieldMapper.findAllField();
    }
}
