package com.boss.bossframeworkweb.service;


import com.boss.bossframeworkweb.pojo.UserLogin;

public interface IUserLoginService{
    /**
     * 登录方法
     */
    UserLogin findUserVoByUsernameAndPassword(UserLogin user);

    /**
     * 注册
     */
    int addPhone(UserLogin login);
}
