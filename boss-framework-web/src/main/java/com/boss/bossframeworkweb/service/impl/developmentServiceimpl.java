package com.boss.bossframeworkweb.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.boss.bossframeworkweb.mapper.companylistMapper;
import com.boss.bossframeworkweb.mapper.developmentMapper;
import com.boss.bossframeworkweb.pojo.company;
import com.boss.bossframeworkweb.pojo.development;
import com.boss.bossframeworkweb.service.companylistService;
import com.boss.bossframeworkweb.service.developmentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class developmentServiceimpl extends ServiceImpl<developmentMapper, development> implements developmentService {
    @Resource
    developmentMapper developmentMapper;

    @Override
    public List<development> findAllDevelopment() {
        return developmentMapper.findAllDevelopment();
    }
}
