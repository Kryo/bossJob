package com.boss.bossframeworkweb.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.boss.bossframeworkweb.pojo.Page;
import com.boss.bossframeworkweb.pojo.company;



import java.util.List;

public interface companylistService extends IService<company> {
    //
    List<company> companyPageList(Page page);

    int totalCount(Integer dId, Integer fId);

}
