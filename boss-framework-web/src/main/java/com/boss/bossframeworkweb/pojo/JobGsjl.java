package com.boss.bossframeworkweb.pojo;/*
 * Welcome to use the TableGo Tools.
 * 
 * http://vipbooks.iteye.com
 * http://blog.csdn.net/vipbooks
 * http://www.cnblogs.com/vipbooks
 * 
 * Author:bianj
 * Email:edinsker@163.com
 * Version:5.8.0
 */

import lombok.Data;
import javax.persistence.Table;

/**
 * 公司简历表
 * 
 * @author bianj
 * @version 1.0.0 2020-06-08
 */
@Data
@Table(name = "job_gsjl")
public class JobGsjl{
    /** id */
    private int id;

    /** 公司名称 */
    private String name;

    /** 职位类别 */
    private String zwlb;

    /** 职位名称 */
    private String zwmc;

    /** 所属部门 */
    private String ssbm;

    /** 月薪范围 */
    private String yxfw;

    /** 工作城市 */
    private String gzcs;

    /** 工作经验 */
    private String gzjy;

    /** 学历要求 */
    private String xlyq;

    /** 职位诱惑 */
    private String zwyh;

    /** 职位描述 */
    private String zwms;

    /** 工作地址 */
    private String gzdz;

    /** 接受邮箱 */
    private String jsyx;

    /** shoucang */
    private String shoucang;

    /** 最高月薪范围 */
    private String zgyxfw;
}