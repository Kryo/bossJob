package com.boss.bossframeworkweb.pojo;/*
 * Welcome to use the TableGo Tools.
 * 
 * http://vipbooks.iteye.com
 * http://blog.csdn.net/vipbooks
 * http://www.cnblogs.com/vipbooks
 * 
 * Author:bianj
 * Email:edinsker@163.com
 * Version:5.8.0
 */

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 邮箱订阅
 * 
 * @author bianj
 * @version 1.0.0 2020-06-08
 */

@Data
@Table(name = "job_search")
public class JobSearch{

    /** id */
    private int id;

    /** email */
    private String email;

    /** day */
    private String day;

    /** name */
    private String name;

    /** site */
    private String site;

    /** development */
    private String development;

    /** domanial */
    private String domanial;

    /** salaried */
    private String salaried;


}