package com.boss.bossframeworkweb.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import javax.persistence.Id;

/**
 * 主页列表3
 */
@Data
@TableName("c")
public class job3 {
    @Id
    private  Integer cid;
    private  String cname;
    private Integer c_id;
}
